const express = require('express');
const app = express();
const port = 8010;
const {Cipher, Decipher} = require('caesar-salad').Vigenere;
const password = 'password';

app.get('/encode/:codeWord', (req, res) => {
    res.send(Cipher(password).crypt(req.params.codeWord));
});

app.get('/decode/:codeWord', (req, res) => {
    res.send(Decipher(password).crypt(req.params.codeWord));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});
