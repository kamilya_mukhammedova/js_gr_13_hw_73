const express = require('express');
const app = express();
const port = 8010;

app.get('/:response', (req, res) => {
    res.send(`${req.params.response}!\n`);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});
